requirejs.config({
  baseUrl: './',
  paths: {
    es6: './es6',
    babel: './babel',
    es3fy: './es3fy',
  },
  es6: {
    fileExtension: '.js' // put in .jsx for JSX transformation
  },

  babel: {
    presets: ['es2015-loose'],
    plugins: ['transform-es2015-modules-amd', 'transform-es3-property-literals', 'transform-es3-member-expression-literals']
  },
});

requirejs(['main']);
